#include <iostream>
#include <fstream>
#include <string>

class Hero{
private:
    std::string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
    
public:
    Hero(std::string new_name, int new_strength, int new_dexterity, int new_endurance, int new_intelligence, int new_charisma);
    
    std::string view_hero();
    
    int save();
    int load();
    
};
